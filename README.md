## Do You Have a Question? I Have an Answer! 

Yes, within reason, I will answer any questions you may have. Not because I am wise or omniscient, hardly, but because I am willing to try to help people with whatever I can. As Gandhi said, "Be the change you want to see in the world," so here I am being the change and being legitimately helpful. 

### How to Ask

Open an issue on this repository and I will answer it in due time, a variable dependent on numerous factors outside of my control at times but I will get to you eventually. 

If I close an issue without an answer... well I won't even the inappropiate will be labeled as such.


---

### Things I Know A Lot About

Here are some topics I know well enough you might want to ask about to receive a useful answer. If you inquire along highly technical lines and I don't know what to tell you, I may do the research for you, or I may answer with "I don't know" but will try to provide you some resources to jumpstart your research. 

**Note:** This is not expansive and will grow as I realize I need to add to it!

- Linux
- Front End Technologies
- Web Development 
- React
- Static Site Generators
- DevOps for personal use
- Homelabs
- Hardware

### Topics That Are Verboten 


> Verboten - German word for forbidden

*So I lied its ask me **ALMOST** anything*

Some things are excluded, because I am working when I am on Github and use tech as a break from certain aspects of life. Despite this eroding away in the civic culture recently, I do this out of respect for the people that disagree and agree with whatever my opinion on the topic is as well as getting a break from the at times oppressive and annoying overload of those topics in one's daily life. Not everything is the end of the world and even if it is, I am working to get a break from that too.

**Note:** This is not expansive and will grow as I realize I need to add to it!

- Politics
- Religion
- History
- Bay Area Gentrification
- Sports Teams
- NSFW Topics 
